### **Anggota Kelompok:**
1. Adira Sayidina - 2006462840
2. Ardhani Dzaky Ralfiano - 2006482905
3. Helga Syahda Elmira - 2006463686
4. Metta Permatasari - 2006463761
5. Mohammad Rizky Chairul Azizi - 2006473421
6. Rahadian Panji Ramadhan - 2006473900
7. Sulthan Afif Althaf - 2006473863

**Link Download APK:** https://ristek.link/donorplasma-a03

### **Latar Belakang dan Manfaat:**

Jumlah kasus baru COVID-19 masih kian bertambah setiap harinya di Indonesia. Walaupun grafik peningkatan kasus ini cenderung menurun tetapi saat ini Indonesia masih menempati terus berjuang menghentikan penyebaran yang terjadi.

Kondisi ini menyebabkan banyak tim peneliti dan tim medis yang berusaha keras mencari alternatif penyembuhan COVID-19. Saat ini ada beberapa pilihan terapi yang bisa dilakukan untuk pasien COVID-19 seperti N-Asetilsistein, Sel Punca Mesenkimal, dan Plasma Konvalesen. Di Indonesia sendiri pemerintah dan PMI sangat mendukung terlaksananya terapi donor plasma konvalesen. Metode ini sangat ramai karena sudah lulus uji etik dan didukung kuat oleh pemerintah serta lembaga kesehatan di Indonesia. Terapi donor plasma konvalesen dilakukan dengan pemberian donor plasma darah penyintas kepada pasien COVID-19 dengan tujuan meningkatkan kadar antibodi pasien sehingga dapat melawan virus COVID-19.

Banyaknya peminat terapi donor plasma konvalesen tidak sebanding dengan jumlah pendonornya. Biasanya, orang-orang yang membutuhkan terapi ini, akan kesulitan menemukan pendonor yang dapat memenuhi persyaratan karena minimnya sumber yang dikenal.

Kelompok kami melihat adanya masalah  bagi para pencari donor yaitu ketersulitan untuk menemukan donor plasma konvalesen yang tepat. Saat ini, penyebaran informasi tentang hal ini masih kurang efektif dengan penyebaran seadanya yakni melalu broadcast media social. 

Maka dari itu, kelompok kami ingin membuat aplikasi yang akan memudahkan pertemuan pencari donor dan calon pendonor. Diharapkan proses pencarian donor plasma konvalesen ini bisa lebih cepat sehingga dapat menekan angka kematian COVID-19 di Indonesia.

Kelompok kami ingin membuat aplikasi yang akan memudahkan pertemuan pencari donor dan calon pendonor. Dengan melakukan pencarian di aplikasi yang menargetkan orang-orang yang berpotensi untuk mendonorkan plasma konvalesen, diharapkan proses pencarian donor plasma konvalesen ini bisa lebih cepat sehingga dapat menekan angka kematian COVID-19 di Indonesia.

### **Fitur dan Cerita Modul:**
    
| Fitur |PIC |
| -------- | -------- |
| Home Page | Ardhani Dzaky Ralfiano |
| Formulir Pendaftaran Pendonor| Sulthan Afif Althaf |
| Formulir Pendaftaran Pencari Donor| Adira Sayidina |
| List UDD terdekat + Contact | Helga Syahda Elmira |
| FAQ | Metta Permatasari |
| Searching (Cari Donor) | Rahadian Panji Ramadhan |
| Artikel | Mohammad Rizky Chairul Azizi |

- **User + Homepage**

  Modul Login/Register dan Navbar berisikan Pengguna yang telah terdaftar pada aplikasi Donor Plasma. Pada page ini akan terdapat opsi login maupun register untuk pengguna baru. 

    Modul ini akan mengimplementasikan beberapa widget seperti padding, theme, appbar, column, container, elevatedButton, scaffold, text widgets, dan lainnya. Pada modul ini akan dilakukan pemanggilan ke Web Service Django, dimana web Service Django akan bertindak sebagai backend database user yang ingin login maupun request. Event handling yang terjadi pada modul ini adalah ketika user klik tombol login atau register.

    Pada modul ini ketika user menekan tombol login maka ia akan login dan ketika ia menekan tomnol register ia akan terdaftar pada aplikasi Donor Plasma. Pada pemanggilan Web Service ini telah dicoba tanpa menggunakan Django dengan Postman dan telah mengembalikan format JSON yang diinginkan. Penerapan tersebut akan diimplementasikan pada Flutter.

- **Formulir Pendaftaran Pendonor**

    Modul form pendaftaran pendonor merupakan implementasi mobile dari form pendaftaran pendonor pada web django sebelumnya. Jika user menekan tombol daftar, akan langsung diarahkan ke screen informasi pendonor. Namun jika pendonor belum pernah terdaftar akan langsung di push screen yang berisi form pendonor. Setelah pengguna mengisi form, data akan diambil dalam bentuk json yang kemudian akan di post ke web service yang tersedia di modul pendonor. Lalu screen form akan di-pop dan kembali ke screen informasi pendonor.
    
    Modul pendonor akan mengimplementasikan widget seperti pada umumnya yaitu text, row, column, button, textformfield, inputdateformfield, dan beberapa lainnya. Modul ini akan melakukan pemanggilan asynchronous pada Web Service proyek Django sebelumnya untuk beberapa kondisi. Kondisi pertama adalah saat melakukan pengecekan apakah sudah ada data pendonor pada user sekarang. Kondisi kedua adalah saat mengambil data pendonor dalam bentuk json yang kemudian akan ditampilkan. Yang terakhir adalah untuk melakukan post data pada web service saat menyimpan data pendonor. 

- **Formulir Pendaftaran Pencari Donor**

    Modul ini digunakan user untuk melakukan pendaftaran mencari donor, tidak ada limitasi pendaftaran untuk setiap akun, dan setiap akun bisa mendaftarkan orang lain. Formulir ini akan meminta informasi mengenai nama, NIK, tanggal lahir, nomor telepon, golongan darah, provinsi, dan kota. Ketika sudah mendaftar, akan ditampilkan screen berisi informasi yang telah diisi, dan akan ada button 'Ubah Data' untuk mengubah data, 'Batal' untuk membatalkan perndaftaran pencarian donor, dan 'Lihat Calon Pendonor' untuk melihat list calon pendonor yang memiliki golongan darah, provinsi, dan kota yang sama dengan pendaftar.

    Modul ini mengimplementasikan berbagai widget, contohnya widget Text, RaisedButton, TextFormField, InputDatePickerFormField, dan lain-lain. Akan ada pemanggilan Asynchonous ke web service django yang akan mengembalikan data json untuk ditampilkan.

- **Informasi UDD**

    Modul Informasi UDD berisikan list UDD yang memiliki stok golongan darah tertentu. Pada page ini akan terdapat cards yang berisikan informasi UDD seperti nama UDD, alamat UDD, nomor telepon UDD, Stok golongan darah yang tersedia beserta jumlahnya. Kemudian, user yang sudah melakukan login dapat menambahkan informasi UDD, mengedit informasi UDD, dan melakukan penghapusan informasi UDD.

    Modul Informasi UDD ini akan mengimplementasikan beberapa widget seperti padding, theme, appbar, column, container, elevatedButton, scaffold, text widgets, dan lain-lain. Pada modul ini akan dilakukan pemanggilan asinkronus ke Web Service Django, dimana Web Service Django akan mengembalikan data response dalam format json yang berupa informasi UDD. Dimodul ini akan ada event handling berupa OnPressed yang akan ter-trigger ketika user menekan button edit, delete, menambahkan UDD, back, dan submit. 

    Pada modul ini ketika ditekan button menambahkan UDD, maka akan tampil form untuk menambahkan informasi UDD baru yang ingin ditambahkan. Kemudian, user dapat menekan tombol back untuk kembali ke page utama atau menekan tombol submit. Dengan menekan tombol submit maka informasi UDD yang baru saja ditambahkan akan muncul sebagai card baru di page utama. Apabila user belum login maka user tidak bisa melakukan penambahan informasi, delete, dan mengedit informasi UDD yang sudah ada.

- **FAQ**

    Modul Frequently Ask Question berisikan list list pertanyaan dan jawaban dari pertanyaan-pertanyaan yang paling sering ditanyakan user. Pada page ini akan terdapat beberapa dropdown berisikan pertanyaan yang apabila di tekan akan memuat jawaban dari pertanyaan-pertanyaan tersebut.

    Apabila terdaftar sebagai admin, maka admin dapat menambahkan, mengubah, maupu menghapus list pertanyaan maupun jawaban tersebut. 

    Modul ini mengimplementasikan beberapa widget, seperti RaisedButton, TextFormField, padding, theme dan lain-lain. Selanjutnya akan dilakukan pemanggilan Asyncronus ke web service django dan kemudian mengembalikan data json untuk kemudian ditampilkan.

- **Cari Donor**

    Modul Cari Donor memungkinkan pengguna yang sudah login untuk mencari pendonor sukarela yang sudah terdaftar pada aplikasi. Pengguna dapat memasukkan lokasi berupa `Provinsi` dan `Kota`, dan `Golongan Darah` dari pendonor yang dicari, kemudian aplikasi akan menampilkan daftar pendonor terdaftar yang berdomisili dan bergolongan darah sesuai yang sudah dimasukkan oleh pengguna.
    
    Dari homepage, pengguna dapat memilih pilihan `Cari Donor` yang akan menavigasikan pengguna ke halaman Cari Donor. Navigasi layar akan diatur dengan Navigator bawaan Flutter. Kemudian pengguna akan diminta untuk mengisi tiga buah dropdown form, yaitu `Provinsi`, `Kota`, dan `Golongan Darah`. Dropdown `Kota` hanya dapat dipilih setelah memilih dropdown `Provinsi` dan hanya memuat kota-kota dalam provinsi yang dipilih. `Provinsi` dan `Kota` yang dapat dipilih diambil melalui API Call ke aplikasi Django yang sudah dibuat saat UTS kemarin. Setelah memilih semua dropdown, pengguna dapat menekan tombol `Cari` untuk mencari pendonor yang sudah difilter berdasarkan tiga kategori tersebut. Aplikasi Flutter akan melakukan API Call ke aplikasi Django membawa informasi filter tersebut dan aplikasi Django akan mengembalikan response berupa daftar pendonor sesuai filter yang diminta tadi. Kemudian data daftar pendonor akan ditampilkan pada aplikasi Flutter. 

- **Artikel**
    
    Modul artikel akan menampilkan artikel-artikel yang berkaitan dengan informasi Covid-19. Masing-masing artikel dapat diakses dan ditampilkan isinya secara detil. Artikel-artikel pada page utama dibentuk dengan widget Card yang diurutkan dalam 1 kolom dengan memanfaatkan widget Column. Kemudian, ada penggunaan widget Listview agar artikel-artikel tersebut dapat di-scroll. Selain itu, juga ada widget Button yang berfungsi untuk melakukan perintah tertentu seperti page routing dari page utama ke page detil artikel dan sebaliknya. Baik judul maupun isi dari artikel akan disusun dengan widget Text. Informasi dari tiap artikel yaitu foto, judul, dan isi merupakan tampilan dari data json hasil pemanggilan asynchonous ke web service proyek Django.




