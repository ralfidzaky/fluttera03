import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  final String lorem =
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.';

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Donor Plasma'),
          backgroundColor: Colors.red[400],
          bottom: TabBar(
            tabs: [
              Tab(text: 'Donor'),
              Tab(text: 'Info UDD'),
              Tab(text: 'FAQ'),
              Tab(text: 'Artikel'),
            ],
          ),
        ),
        body: TabBarView(children: [
          Center(
            child: Text('Page 1'),
          ),
          Center(
            child: Text('Page 2'),
          ),
          Center(
            child: Text('Page 3'),
          ),
          Center(
            child: Text('Page 4'),
          ),
        ]),
      ),
    );
  }
}
