import 'package:flutter/material.dart';
import 'package:fluttera03/landingPage/loginPage.dart';
import 'package:fluttera03/landingPage/registrationPage.dart';

class LogRegPage extends StatelessWidget {
  const LogRegPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: 200,
            height: 200,
            child: Image(
              image: AssetImage('images/icon.png'),
              fit: BoxFit.contain,
            ),
            margin: EdgeInsets.all(10),
          ),
          Container(
            margin: EdgeInsets.all(10),
            child: Text(
              'Donor Plasma',
              style: TextStyle(
                fontSize: 35,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.all(5),
                child: ElevatedButton(
                    key: Key('loginButtonLogRegPage'),
                    style: ElevatedButton.styleFrom(
                      primary: Colors.red[400],
                    ),
                    onPressed: () {
                      Navigator.pushReplacement(context,
                          MaterialPageRoute(builder: (context) {
                        return LoginPage();
                      }));
                    },
                    child: Text('Login')),
              ),
              Container(
                margin: EdgeInsets.all(5),
                child: ElevatedButton(
                    key: Key('regButtonLogRegPage'),
                    style: ElevatedButton.styleFrom(
                      primary: Colors.red[400],
                    ),
                    onPressed: () {
                      Navigator.pushReplacement(context,
                          MaterialPageRoute(builder: (context) {
                        return RegistrationPage();
                      }));
                    },
                    child: Text('Register')),
              )
            ],
          ),
        ],
      ),
    ));
  }
}
