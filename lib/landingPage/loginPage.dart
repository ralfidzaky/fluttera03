import 'package:flutter/material.dart';
import 'package:fluttera03/landingPage/homePage.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _loginState createState() => _loginState();
}

class _loginState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    final namaUser = TextEditingController();
    final passUser = TextEditingController();
    bool _validateNama = false;
    bool _validatePass = false;

    void loggedIn() {
      AlertDialog alertDialog = AlertDialog(
        content: Container(
          height: 100,
          child: Column(
            children: [
              Container(
                  margin: EdgeInsets.all(10),
                  child: Text("You have successfully logged in!")),
              Container(
                margin: EdgeInsets.all(10),
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Colors.red[400],
                    ),
                    onPressed: () {
                      Navigator.pushReplacement(context,
                          MaterialPageRoute(builder: (context) {
                        return HomePage();
                      }));
                    },
                    child: Text('Ok')),
              )
            ],
          ),
        ),
      );
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return alertDialog;
          });
    }

    void clearField() {
      namaUser.clear();
      passUser.clear();
    }

    return Scaffold(
        body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
              child: TextField(
                controller: namaUser,
                maxLength: 25,
                decoration: InputDecoration(
                    hintText: 'Username',
                    errorText:
                        _validateNama ? 'Please input your username' : null,
                    labelText: 'Username',
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(32.0))),
              ),
              margin: EdgeInsets.fromLTRB(30, 10, 30, 10)),
          Container(
            child: TextField(
              obscureText: true,
              enableSuggestions: false,
              autocorrect: false,
              controller: passUser,
              decoration: InputDecoration(
                  hintText: 'Password',
                  errorText:
                      _validatePass ? 'Please input your password' : null,
                  labelText: 'Password',
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(32.0))),
            ),
            margin: EdgeInsets.fromLTRB(30, 10, 30, 10),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(30, 10, 30, 10),
            child: ElevatedButton(
                onPressed: () {
                  setState(() {
                    namaUser.text.isEmpty
                        ? _validateNama = true
                        : _validateNama = false;
                    passUser.text.isEmpty
                        ? _validatePass = true
                        : _validatePass = false;
                    if (_validatePass == false && _validateNama == false) {
                      loggedIn();
                      clearField();
                    }
                  });
                },
                child: Text('Login')),
          ),
        ],
      ),
    ));
  }
}
